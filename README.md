# Название проекта

Проект для курса [My First Data Project 2](https://ods.ai/tracks/mfdp2).

## Описание проекта

Проект разделен на два репозитория. 
Данный репозиторий позволяет с помощью docker-compose поднять два сервиса, первый -  api ML-модели для распознавания эмоций в тексте на основе fast-api, а второй сервис - демо модели на основе flask и bootstrap.

В репозитории [mfdp-sentiment](https://gitlab.com/ViktorVlasov/mfdp-sentiment) реализована часть с обучением модели с использованием gpu в докере.

## Демо модели

Демонстрация модели доступна по адресу: http://94.198.217.232:8003/demo

## Описание проведенных экспериментов



## Использование модели из MLFlow Registry

- Одна из обученных и залоггированных моделей через MLFlow регистрируется в MLFlow Registry с названием prod_model и стейджем Production, если какая-либо модель с таким названием и стадией уже была зарегистрирована, то она уходит в архив.
- При сборке сервисов с помощью docker-compose модель API инициализируется моделью из MLFlow Registry с названием prod_model и стейджем Production.
## Учетные данные для авторизации, шифрование git-crypt

Переменные окружения с учетными данными для авторизации в MLflow хранятся в файле `.env`. Для версионирования и хранения данных в проекте используется DVC S3 с s3 бакетом на VK Cloud.  
Файл с учетными данными (.env) зашифрованы с использованием git-crypt.
## Минимальные требования

- Docker CE
- Docker-compose
- git-crypt

## Шаги по установке проекта

1. Склонируйте репозиторий:
```bash
git clone https://gitlab.com/ViktorVlasov/mfdp-sentiment-api
```
2. Установите [git-crypt](https://github.com/AGWA/git-crypt). В зависимости от вашей операционной системы, выполните одну из следующих команд:
   - Для macOS: `brew install git-crypt`
   - Для Ubuntu: `sudo apt install git-crypt`

3. Получите ссылку на файл `git-crypt-key` по запросу (напишите мне в [телеграм](https://t.me/rhythm_00)). Если вы являетесь одним из менторов MFDP 2, то найдите мой ответ на последний модуль "Подготовка питчинга". В нем я прикрепил ссылку на файл. Установите переменную окружения `GIT_CRYPT_KEY_URL` со значением ссылки на файл:
```bash
export GIT_CRYPT_KEY_URL="ссылка на файл git-crypt-key"
```
## Развертывание сервисов

1. Для развертывания сервисов вам нужно: 
- Выполнить:
```bash
chmod +x docker_start.sh
./docker_start.sh
```

***Дополнительно:*** если сменилась модель с стейджем prod в MLFlow Registry, нужно пересобрать контейнер и проинициализировать модель заново:
```bash
docker-compose up -d --no-deps --build api
```

## Организация проекта
------------

    ├── LICENSE
    ├── README.md          <- The top-level README.
    │
    │
    ├── pyproject.toml     <- The specified file format of PEP 518 which contains 
    │                         the build system requirements of Python projects.
    ├── poetry.lock        <- The poetry.lock file prevents you from automatically 
    │                         getting the latest versions of your dependencies.
    │
    ├── src                <- Source code for use in this project.
    │   │
    │   ├── api
    │   │   ├── __init__.py    <- Makes src a Python module
    │   │   │
    │   │   ├── configs        <- Yacs base config script and yaml config.
    │   │   │   └── base_config.py
    │   │   │
    │   │   ├── models         <- Classes that implement models
    │   │   │   ├── EmotionClassifier.py
    │   │   │
    │   │   ├── api.py       <-  Initial FastAPI service
    │   │   
    │   │   
    │   ├── web
    │       │
    │       ├── static      <- Static files for demo (.css)
    │       │
    │       ├── templates   <- HTML templates for demo site
    │       │
    │       ├── app.py  <-  Initial Flask service
    │   
    ├── dvc.lock           
    ├── dvc.yaml
    ├── .dvc    
    │
    ├── Dockerfile         
    ├── .dockerignore      
    ├── docker_start.sh    
    │     
    ├── .gitattributes     
    ├── .gitignore         
    │
    ├── .env
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
