import os
from typing import List

import gradio as gr
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

from src.models.EmotionClassifier import EmotionClassifierProd

LOCALPATH_ART = '/app/artifacts'
MAX_TEXTS = 30

class TextData(BaseModel):
    texts: List[str]

def model_init():
    model_name = os.getenv('MODEL_NAME')
    stage = os.getenv('MODEL_STAGE')
    if model_name is None or stage is None:
        raise Exception("Missing MODEL_NAME or MODEL_STAGE env variable. Add to .env")

    model = EmotionClassifierProd(model_name=model_name,
                        stage=stage,
                        localpath_art=LOCALPATH_ART,
                        overwrite=False)
    return model

app = FastAPI()
model = model_init()

@app.post("/invocations")
async def get_predict(text_data: TextData):
    texts = text_data.texts
    if len(texts) > MAX_TEXTS:
        raise HTTPException(status_code=404,
                            detail=f"Too many sentence, reduce to {MAX_TEXTS}.")
    emotions = []
    for text in texts:
        emotion = model.predict(text)[0]
        emotions.append(emotion)
    result = [{'text': t, 'emotion': e} for t, e in zip(texts, emotions)]
    return result

def get_predict_gradio(text: str):
    proba_dict = model.predict(text)[1]
    return proba_dict

demo = gr.Interface(
    fn=get_predict_gradio,
    inputs=gr.Textbox(placeholder="Введите ваше предложение..."),
    outputs="label",
    allow_flagging = "never",
    examples=[
        ["Этот мир прекрасен до невозможности!"],
        ["Меня слишком сильно поразили события этого года"],
        ["Привет мир!"]
    ])

CUSTOM_PATH = "/demo"
app = gr.mount_gradio_app(app, demo, path=CUSTOM_PATH)
