"""
Basic configuration file.
It is suggested to use YACS config (https://github.com/rbgirshick/yacs ) to
support the basic configuration file. Then, for each specific
experiment, you can create a configuration file that will override
the necessary parameters of the basic configuration file.


Usage example:
    1. Create a configuration file for a specific experiment
    (for example, configs/experiment_1.yaml)
    2. In the configuration file, redefine the necessary parameters
    basic configuration file
    3. In the module where it is necessary to use the configuration parameters,
    import the combine_config function
    4. Call the combine_config function by passing the path as an argument
    to the configuration file of a specific experiment
    5. The resulting yacs CfgNode object can be used to access
    to the configuration parameters
"""

import os.path as osp
from datetime import datetime
from typing import Optional, Union

from yacs.config import CfgNode as CN

_C = CN()

# Root directory of project
_C.ROOT = CN()
_C.ROOT.PATH = '/Users/your_username/mfdp-sentiment'

# Dataset emotions
_C.DATASET_EMO = CN()
_C.DATASET_EMO.RAW_PATH = 'data/raw/emotions/six_emotions.csv'
_C.DATASET_EMO.SPLIT_OUTPUT_PATH = 'data/interim/emotions'
_C.DATASET_EMO.TEST_SIZE = 0.2
_C.DATASET_EMO.VAL_SIZE = 0.2
_C.DATASET_EMO.BALANCE_CLASSES = False

# Datamodule
_C.DATAMODULE = CN()
_C.DATAMODULE.TRAIN_PATH = 'train.csv'
_C.DATAMODULE.TEST_PATH = 'test.csv'
_C.DATAMODULE.VAL_PATH = 'val.csv'
_C.DATAMODULE.BATCH_SIZE = 32
_C.DATAMODULE.MAX_LENGTH = 128
_C.DATAMODULE.NUM_WORKERS = 2

# Learning
_C.LEARNING = CN()
_C.LEARNING.BASE_MODEL = 'DeepPavlov/rubert-base-cased'
_C.LEARNING.CHECKPOINT_PATH: Optional[str] = None
_C.LEARNING.ETA = 3e-2
_C.LEARNING.MAX_EPOCHS = 10
_C.LEARNING.DEVICE = 'cpu'
_C.LEARNING.NUM_CLASSES = 6
_C.LEARNING.SEED = 42

# MLFLOW
_C.MLFLOW = CN()
_C.MLFLOW.TRACKING_URI = ''
_C.MLFLOW.S3_ENDPOINT_URL = ''
_C.MLFLOW.EXPERIMENT = ''
_C.MLFLOW.LOG_CHECKPOINTS = True
_C.MLFLOW.LOG_MODEL = True
_C.MLFLOW.RUN_NAME = f'{datetime.now().strftime("%yy_%mm_%dd_%Hh_%Mm_%Ss")}'

#Logging
_C.LOGGING = CN()
_C.LOGGING.LOGS_FOLDER = 'logs'
_C.LOGGING.EXPERIMENT_FOLDER = f'{datetime.now().strftime("%y_%m_%d_%H_%M")}'
_C.LOGGING.MODEL_NAME = f'{_C.LEARNING.BASE_MODEL}.pt'
_C.LOGGING.LOGGING_INTERVAL = 'step'

#Checkpoint
_C.CHECKPOINT = CN()
_C.CHECKPOINT.CKPT_FOLDER = 'checkpoints'
_C.CHECKPOINT.FILENAME = '{epoch}_{valid_acc:.2f}_{valid_loss:.2f}'
_C.CHECKPOINT.SAVE_TOP_K = 2
_C.CHECKPOINT.CKPT_MONITOR = 'valid_loss'
_C.CHECKPOINT.CKPT_MODE = 'min'

# Early stopping
_C.ES = CN()
_C.ES.MONITOR = 'valid_loss'
_C.ES.MIN_DELTA = 2e-4
_C.ES.PATIENCE = 10
_C.ES.VERBOSE = False,
_C.ES.MODE = 'min'

_C.CFG_INFO = CN()
_C.CFG_INFO.CFG_PATH = 'src/configs/params.yaml'


def get_cfg_defaults():
    """Returns a yacs CfgNode object with default values"""
    return _C.clone()


def combine_config(cfg_path: Union[str, None] = None):
    """
    Combines the basic configuration file with
    the configuration file of a specific experiment
    Args:
         cfg_path (str): file in .yaml or .yml format with
         config parameters or None to use Base config
    Returns:
        yacs CfgNode object
    """
    base_config = get_cfg_defaults()
    if cfg_path is not None:
        if osp.exists(cfg_path):
            base_config.merge_from_file(cfg_path)
        else:
            raise FileNotFoundError(f'File {cfg_path} does not exists')

    # Join paths dataset
    base_config.DATASET_EMO.RAW_PATH = osp.join(
        base_config.ROOT.PATH,
        base_config.DATASET_EMO.RAW_PATH
    )
    base_config.DATASET_EMO.SPLIT_OUTPUT_PATH = osp.join(
        base_config.ROOT.PATH,
        base_config.DATASET_EMO.SPLIT_OUTPUT_PATH
    )

    # Join paths datamodule
    base_config.DATAMODULE.TRAIN_PATH = osp.join(
        base_config.DATASET_EMO.SPLIT_OUTPUT_PATH,
        base_config.DATAMODULE.TRAIN_PATH
    )
    base_config.DATAMODULE.TEST_PATH = osp.join(
        base_config.DATASET_EMO.SPLIT_OUTPUT_PATH,
        base_config.DATAMODULE.TEST_PATH
    )
    base_config.DATAMODULE.VAL_PATH = osp.join(
        base_config.DATASET_EMO.SPLIT_OUTPUT_PATH,
        base_config.DATAMODULE.VAL_PATH
    )

    base_config.CFG_INFO.CFG_PATH = osp.join(
        base_config.ROOT.PATH,
        base_config.CFG_INFO.CFG_PATH
    )

    if base_config.LEARNING.CHECKPOINT_PATH:
        base_config.LEARNING.CHECKPOINT_PATH = osp.join(
            base_config.ROOT.PATH,
            base_config.LEARNING.CHECKPOINT_PATH
        )

    base_config.LOGGING.EXPERIMENT_PATH = osp.join(
        base_config.ROOT.PATH,
        base_config.LOGGING.LOGS_FOLDER,
        base_config.MLFLOW.EXPERIMENT
    )
    base_config.LOGGING.RUN_PATH = osp.join(
        base_config.LOGGING.EXPERIMENT_PATH,
        base_config.MLFLOW.RUN_NAME
    )
    base_config.LOGGING.SAVING_MODEL_PATH = osp.join(
        base_config.LOGGING.RUN_PATH,
        base_config.LOGGING.MODEL_NAME
    )
    base_config.CHECKPOINT.CKPT_PATH = osp.join(
        base_config.LOGGING.RUN_PATH,
        base_config.CHECKPOINT.CKPT_FOLDER
    )

    return base_config
