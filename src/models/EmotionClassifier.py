import os
from pathlib import Path

import mlflow
import torch
from torch import nn
from transformers import AutoTokenizer

from src.configs.base_config import combine_config


class EmotionClassifier(nn.Module):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def forward(self, input_ids: torch.Tensor, attention_mask: torch.Tensor):
        """
        Forward pass of the emotion classifier.

        Args:
            input_ids (torch.Tensor): Input token IDs.
            attention_mask (torch.Tensor): Attention mask for input.

        Returns:
            torch.Tensor: Logits for each class.
        """
        outputs = self.model(input_ids=input_ids, attention_mask=attention_mask)
        return outputs.logits

class EmotionClassifierProd():
    """
    A class for emotion classification in production.

    Args:
        model_name (str): The name of the model to be loaded.
        stage (str, optional): The stage of the model. Default is 'Production'.
        localpath_art (str, optional): The local path to store the downloaded artifacts.
            Default is '/root/app/artifacts'.
        overwrite (bool, optional): Flag indicating whether to overwrite existing
        artifacts.
            Default is False.

    Attributes:
        model_name (str): The name of the model.
        stage (str): The stage of the model.
        localpath_art (Path): The local path object for storing artifacts.
        overwrite (bool): Flag indicating whether to overwrite existing artifacts.
        device (str): The device to be used for inference.
        model: The pre-trained emotion classification model.
        le: The label encoder for converting predicted indices to emotion classes.
        cfg: The configuration object for the model.
        tokenizer: The tokenizer used for text encoding.

    Methods:
        setup() -> None:
            Performs the setup steps for the classifier.
        download_artifacts() -> None:
            Downloads the required artifacts.
        get_source_artifacts() -> str:
            Retrieves the MLFLOW source of the artifacts.
        predict(texts: List[str]) -> List[str]:
            Performs emotion classification on the input texts and
            returns the predicted emotion classes.
    """
    def __init__(self,
                 model_name: str,
                 stage: str = 'Production',
                 localpath_art: str = '/root/app/artifacts',
                 overwrite: bool = False) -> None:
        self.model_name = model_name
        self.stage = stage
        self.localpath_art = Path(localpath_art)
        self.overwrite = overwrite

        self.device = os.getenv('DEVICE_INFERENCE_MODEL')
        if self.device is None:
            self.device = 'cpu'
        print(f"Device is '{self.device}'")
        self.setup()

    def setup(self) -> None:
        """
        Performs the setup steps for the classifier.
        - Downloads the required artifacts including model weights, label encoder
        and configuration file.
        - Initializes the tokenizer.
        """
        self.download_artifacts()

        self.model = torch.load(self.localpath_art / 'model/data/model.pth',
                                map_location = self.device)
        self.le = torch.load(self.localpath_art / 'label_encoder.pth')
        self.cfg = combine_config(self.localpath_art / 'params.yaml')
        self.tokenizer = AutoTokenizer.from_pretrained(self.cfg.LEARNING.BASE_MODEL)

    def download_artifacts(self) -> None:
        """
        Downloads the required artifacts from the source.
        - Skips downloading if an artifact already exists and overwrite=False.
        """
        source = self.get_source_artifacts()

        artifacts = [
            'label_encoder.pth',
            'model/data/model.pth',
            'params.yaml'
        ]

        os.makedirs(self.localpath_art / 'model/data/', exist_ok=True)
        for artifact in artifacts:
            artifact_uri = source + artifact
            local_path_artifact = self.localpath_art / artifact

            if not self.overwrite and (local_path_artifact).exists():
            # Артефакт уже существует и флаг overwrite=False, пропускаем скачивание
                continue

            mlflow.artifacts.download_artifacts(artifact_uri=artifact_uri,
                                                dst_path=local_path_artifact.parent)

    def get_source_artifacts(self) -> str:
        """
        Retrieves the source of the artifacts for the specified model and stage.

        Returns:
            str: The source URL of the artifacts.

        Raises:
            Exception: If the specified model and stage are not found.
        """
        client = mlflow.MlflowClient()
        for mv in client.search_model_versions(f"name='{self.model_name}'"):
            dict_mv = dict(mv)
            if dict_mv['current_stage'] == self.stage:
                return dict_mv['source'].split('model')[0]
        raise Exception(f"Model '{self.model_name}:{self.stage}' not found")

    def predict(self, text: str):
        """
        Performs emotion classification on the input text.

        Args:
            text (str): A text to classify.

        Returns:
            Tuple[str, Dict]:
            A tuple containing a predicted emotion class corresponding to the input text
            and a dict of corresponding probabilities for all emotions.
        """
        encoding = self.tokenizer.encode_plus(
            text,
            add_special_tokens=True,
            max_length=self.cfg.DATAMODULE.MAX_LENGTH,
            padding='max_length',
            truncation=True,
            return_tensors='pt'
        )

        input_ids = encoding['input_ids'].to(self.device)
        attention_mask = encoding['attention_mask'].to(self.device)

        self.model.eval()
        with torch.inference_mode():
            outputs = self.model(input_ids, attention_mask)
            probabilities = nn.functional.softmax(outputs, dim=1).detach()

        max_index = torch.argmax(outputs)
        predict_class = self.le.inverse_transform([max_index])
        proba_rounded_values = [round(float(value), 2) for value in probabilities[0]]
        classes = self.le.classes_
        proba_dict = {k: v for k, v in zip(classes, proba_rounded_values)}

        return predict_class[0], proba_dict
