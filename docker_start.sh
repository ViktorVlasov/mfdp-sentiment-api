#!/bin/bash

# Получаем абсолютный путь к скрипту
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# Проверка наличия файла git-crypt-key и дешифрование credentials
GIT_CRYPT_KEY_FILE="$SCRIPT_DIR/git-crypt-key"
if [ ! -f $GIT_CRYPT_KEY_FILE ]; then
  # Файл git-crypt-key отсутствует, скачиваем его
  if [ -z "$GIT_CRYPT_KEY_URL" ]; then
    echo "Error: GIT_CRYPT_KEY_URL variable is not set."
    exit 1
  fi
  echo "Downloading git-crypt-key file..."
  wget --no-check-certificate "$GIT_CRYPT_KEY_URL" -O "$GIT_CRYPT_KEY_FILE"
  echo "git-crypt-key file downloaded."
  git-crypt unlock $GIT_CRYPT_KEY_FILE
fi

docker-compose up -d --build